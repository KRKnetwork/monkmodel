const Schema = require('./schema')
const monk = require('monk')
const deepmerge = require('deepmerge')
const cloneDeep = require('lodash.clonedeep')

module.exports = class BaseModel {
  constructor (data = {}, isNew = true) {
    this.data = this._sanitize(data)
    this._isNew = isNew
  }

  static get model () { return undefined }

  static get isObjectID () { return this._modelSchema._id === undefined }

  static get _baseSchema () {
    return new Schema({
      _id: { type: String },
      _created: { type: Date, default: () => new Date() },
      _lastChange: { type: Date, default: () => new Date() }
    })
  }

  static get _modelSchema () { return {} }

  static get schema () { return new Schema(this._baseSchema, this._modelSchema) }

  get title () {
    return this.data._id
  }

  static async get (search, key = '_id') {
    switch (typeof search) {
      case 'undefined':
        // All Entries
        return (await this.model.find({})).map(d => new this(d, false))
      case 'string':
      case 'number':
        // By ID return Single Element
        if (this.isObjectID && key === '_id') search = this.objectID(search)
        let result = await this.model.findOne({ [key]: search })
        return result === null ? undefined : new this(result, false)
      case 'object':
        // By Filter
        if (this.isObjectID && search['_id']) search['_id'] = this.objectID(search['_id'])
        return (await this.model.find(search)).map(d => new this(d, false))
      default:
        // Unsupported Fallback
        return []
    }
  }

  toJSON () {
    return cloneDeep(this.data)
  }

  update (data) {
    const id = this.data._id
    this.data = this._sanitize({ ...data, _id: id })

    return this
  }

  patch (data) {
    const id = this.data._id
    this.data = { ...this._sanitize(deepmerge(this.data, data, { arrayMerge: (_destination, source) => source })), _id: id } // KeepID

    return this
  }

  static validate (input) {
    return this.schema.validate(input)
  }

  async delete () {
    await this.constructor.model.remove({ _id: this._id })

    return true
  }

  async save (silent = false) {
    if (!silent) this.data._lastChange = new Date()

    if (this._isNew) {
      return this.constructor.model.insert({ ...this.data })
        .then(newData => {
          this._isNew = false
          this.data = newData
        })
        .catch(err => {
          if (err.code !== 11000) throw err
          throw new Error('_id already set')
        })
        .finally(() => this)
    } else {
      this.data = await this.constructor.model.findOneAndUpdate({ _id: this._id }, stripID(this.data), { replaceOne: true })

      return this
    }
  }

  get _id () {
    if (this.data._id === undefined) return
    return this.constructor.isObjectID ? this.constructor.objectID(this.data._id) : this.data._id
  }

  _sanitize (data = {}) {
    return this.constructor.schema.parse(data)
  }

  static objectID (id) {
    try {
      return monk.id(id)
    } catch (_err) {
      return id
    }
  }
}

const stripID = ({ _id, ...rest }) => rest
